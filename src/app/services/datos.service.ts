import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DatosService {

  private baseUrl = 'https://gorest.co.in/public/v1/todos';

  constructor(private _http: HttpClient) { }

  getTareas(): Observable<any> {
    return this._http.get('https://gorest.co.in/public/v1/todos');
  }
  
}
