import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { getTestBed, TestBed } from '@angular/core/testing';

import { DatosService } from './datos.service';


const stubTareas = [
  { id: 11, user_id: 6, title: "Desino conicio asperiores denego curiositas similique ut cui statua.", due_on: "2021-10-06T00:00:00.000+05:30", status: "completed" },
  { id: 2065, user_id: 1512, title: "Mrs. Oscar Gibson", due_on: null, status: "pending" },
  { id: 68, user_id: 44, title: "Alias bonus balbus artificiose video.", due_on: "2021-10-09T00:00:00.000+05:30", status: "pending" },
  { id: 69, user_id: 44, title: "Urbanus unus delicate vitium triumphus demulceo vespillo libero amissio.", due_on: "2021-10-10T00:00:00.000+05:30", status: "completed" },
  { id: 70, user_id: 44, title: "Cattus turpe taceo thema tumultus.", due_on: "2021-09-23T00:00:00.000+05:30", status: "completed" },
  { id: 90, user_id: 56, title: "Comminor verto cui avaritia defluo cervus tergo doloribus textus.", due_on: "2021-10-14T00:00:00.000+05:30", status: "pending" },
  { id: 91, user_id: 56, title: "Arbustum audentia tabula laborum arceo cruciamentum sollicito dolore sumptus voluptas.", due_on: "2021-09-24T00:00:00.000+05:30", status: "completed" },
  { id: 92, user_id: 56, title: "Campana quos sol arto esse.", due_on: "2021-10-10T00:00:00.000+05:30", status: "pending" },
  { id: 97, user_id: 59, title: "Timidus civis cruciamentum corroboro conscendo utrimque deprimo ulciscor fuga templum.", due_on: "2021-10-06T00:00:00.000+05:30", status: "pending" },
  { id: 98, user_id: 59, title: "Admitto votum degero tersus solum volva eum praesentium.", due_on: "2021-10-10T00:00:00.000+05:30", status: "completed" },
  { id: 99, user_id: 59, title: "Deludo curvus venustas callide minus textus corona.", due_on: "2021-10-04T00:00:00.000+05:30", status: "completed" },
  { id: 100, user_id: 60, title: "Abbas itaque articulus aut cunabula eveniet thema aut audax temptatio.", due_on: "2021-10-21T00:00:00.000+05:30", status: "completed" },
  { id: 101, user_id: 60, title: "Culpo sit vester cultura sodalitas addo censura.", due_on: "2021-10-04T00:00:00.000+05:30", status: "pending" },
  { id: 105, user_id: 62, title: "Exercitationem termes deduco crinis trans demoror amo.", due_on: "2021-10-01T00:00:00.000+05:30", status: "pending" },
  { id: 106, user_id: 62, title: "Audeo territo tempora velum tero argumentum.", due_on: "2021-10-09T00:00:00.000+05:30", status: "pending" },
  { id: 107, user_id: 63, title: "Peior adaugeo adhaero adflicto deludo vobis vestigium capillus vestrum.", due_on: "2021-09-23T00:00:00.000+05:30", status: "completed" },
  { id: 116, user_id: 69, title: "Vindico thermae uberrime conscendo cui deficio audax thymbra tutis enim verbum.", due_on: "2021-09-25T00:00:00.000+05:30", status: "pending" },
  { id: 117, user_id: 69, title: "Sunt audeo sublime deserunt casso triduana depereo.", due_on: "2021-10-13T00:00:00.000+05:30", status: "completed" },
  { id: 118, user_id: 71, title: "Aspicio succurro animi claudeo colo celo unus suasoria abutor abbas.", due_on: "2021-10-15T00:00:00.000+05:30", status: "pending" },
  { id: 119, user_id: 71, title: "Decens delectatio antepono defessus casso.", due_on: "2021-09-28T00:00:00.000+05:30", status: "pending" }]

describe('DatosService', () => {
  let testBed: TestBed;
  let httpMock: HttpTestingController;
  let service: DatosService;


  beforeEach(() => {
    TestBed.configureTestingModule({
      // Tenemos que configurar el módulo para que funcione
      // imports y providers
      imports: [HttpClientTestingModule],
      providers: [DatosService]
    });

    // Inicializamos las variables
    testBed = getTestBed();
    httpMock = testBed.inject(HttpTestingController)
    service = testBed.inject(DatosService);
  });

  afterEach(() => {
    httpMock.verify(); // Saber si se ha ejecutado o no el mock de http
  });


   it('puede cargar el servicio de cargar las tareas', () => {
     service.getTareas().subscribe(
       (respuesta) => {
         expect(respuesta).toBe(stubTareas);
       }
     );

     const peticion = httpMock.expectOne('https://gorest.co.in/public/v1/todos');
     peticion.flush(stubTareas);
     expect(peticion.request.method).toBe('GET');
   });

});
