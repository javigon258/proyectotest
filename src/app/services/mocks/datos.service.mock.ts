import { Observable, of } from 'rxjs';

/**
 *
 * Dado que ListaAlumnosComponent depende de AlumnosService
 * Vamos a crear un STUB que simule los métodos que
 * tendrá/ía el AlumnosService para que en los tests del componente
 * ListaAlumnosComponent no dependamos del Servicio Real y así
 * centremos las pruebas unitarias en el componente y no dependan
 * de sus dependencias.
 *
 */
export class TareasServiceStub {
  // Simulamos lo que devuelve el método getAlumnos
  getAlumnos(): Observable<any> {
    return of({
      data: [
        {
          id: 115,
          user_id: 77,
          title: "Aut vere sortitus conservo comes theca debilito umerus.",
          due_on: "2021-09-21T00:00:00.000+05:30",
          status: "pending"
        },
        {
          id: 116,
          user_id: 79,
          title: "Id dignissimos adnuo officia adduco ea tolero ut conscendo.",
          due_on: "2021-09-29T00:00:00.000+05:30",
          status: "pending"
        },
        {
          id: 117,
          user_id: 79,
          title: "Utilis comminor considero atrocitas deleniti antea tametsi speciosus.",
          due_on: "2021-09-30T00:00:00.000+05:30",
          status: "pending"
        },
        {
          id: 121,
          user_id: 81,
          title: "Territo crustulum tunc sed censura cum arcus teres abutor ultra comis.",
          due_on: "2021-09-22T00:00:00.000+05:30",
          status: "pending"
        },
        {
          id: 122,
          user_id: 81,
          title: "Eos conventus custodia delego adulatio deprecator corrumpo stillicidium hic sunt deludo.",
          due_on: "2021-10-07T00:00:00.000+05:30",
          status: "pending"
        },
        {
          id: 123,
          user_id: 81,
          title: "Tutis deprimo carmen cui laborum suppono volup.",
          due_on: "2021-10-01T00:00:00.000+05:30",
          status: "pending"
        },
        {
          id: 124,
          user_id: 82,
          title: "Aliquam thema tres dignissimos tego.",
          due_on: "2021-10-17T00:00:00.000+05:30",
          status: "pending"
        },
        {
          id: 125,
          user_id: 82,
          title: "Cervus thesis turba cena sed vitae adicio convoco.",
          due_on: "2021-10-17T00:00:00.000+05:30",
          status: "completed"
        },
        {
          id: 126,
          user_id: 82,
          title: "Autem enim attonbitus cohors suscipit debeo vir non.",
          due_on: "2021-10-13T00:00:00.000+05:30",
          status: "pending"
        },
        {
          id: 127,
          user_id: 83,
          title: "Alveus umerus attero tam ars stillicidium suffragium arbor cotidie appositus.",
          due_on: "2021-09-29T00:00:00.000+05:30",
          status: "completed"
        },
        {
          id: 128,
          user_id: 83,
          title: "Ulciscor cum curatio perspiciatis suscipio.",
          due_on: "2021-10-15T00:00:00.000+05:30",
          status: "completed"
        },
        {
          id: 129,
          user_id: 83,
          title: "Tabula caste autem degusto valetudo animi corpus eligendi capillus patruus.",
          due_on: "2021-10-03T00:00:00.000+05:30",
          status: "pending"
        },
        {
          id: 130,
          user_id: 84,
          title: "Decor virtus degusto accedo quis suasoria suppono communis.",
          due_on: "2021-10-01T00:00:00.000+05:30",
          status: "completed"
        },
        {
          id: 131,
          user_id: 84,
          title: "Confugo subito tum summisse sonitus.",
          due_on: "2021-10-04T00:00:00.000+05:30",
          status: "completed"
        }
      ],
    });
  }

  // Simulamos el lo que devuelve el método getAlumno(id)
  // Daría igual que el id sea 1, 2, 3, etc. que siempre devuleve el mismo alumno
  getAlumno(id: number): Observable<any> {
    return of({
      data: {
        id: 1,
        email: 'george.bluth@reqres.in',
        first_name: 'George',
        last_name: 'Bluth',
        avatar: 'https://reqres.in/img/faces/1-image.jpg',
      },
    });
  }
}
