export class Datos{
    
    constructor(
        public id: number,
        public user_id: number,
        public title:string,
        public due_on: string,
        public status: string
    ){

    }
}