import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, NgControl, ReactiveFormsModule } from '@angular/forms';
import { DatosService } from 'src/app/services/datos.service';
import { TareasServiceStub } from 'src/app/services/mocks/datos.service.mock';
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { HomeComponent } from './home.component';
import { Datos } from 'src/app/models/datos';

const stubTareas = [
  { id: 11, user_id: 6, title: "Desino conicio asperiores denego curiositas similique ut cui statua.", due_on: "2021-10-06T00:00:00.000+05:30", status: "completed" },
  { id: 2065, user_id: 1512, title: "Mrs. Oscar Gibson", due_on: null, status: "pending" },
  { id: 68, user_id: 44, title: "Alias bonus balbus artificiose video.", due_on: "2021-10-09T00:00:00.000+05:30", status: "pending" }
];
let tarea: {
  id: 1;
  user_id: 79;
  title: 'Utilis comminor considero atrocitas deleniti antea tametsi speciosus.';
  due_on: '2021-09-30T00:00:00.000+05:30';
  status: 'pending';
};

describe('HomeComponent Metodo de crear tarea', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let httpMock: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HomeComponent],
      imports: [
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule
      ],
      providers: [DatosService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Debe existir el HomeComponent',()=>{

    expect(component).toBeTruthy();
  });

  it('should call to anadirTarea', () => {
    let cantidad = component.tareas.length;
    expect(component.tareas.length).toBe(cantidad);
    component.crearTarea();

    let id = component.contacto.controls['id'];
    let user_id = component.contacto.controls['user_id'];
    let title = component.contacto.controls['title'];
    let due_on = component.contacto.controls['due_on'];
    let status = component.contacto.controls['status'];

    id.setValue(1);
    user_id.setValue(79);
    title.setValue('Utilis comminor considero atrocitas deleniti antea tametsi speciosus.');
    due_on.setValue('2021-09-30T00:00:00.000+05:30');
    status.setValue('pending');
    
    expect(component.tareas.length).toBeGreaterThan(cantidad);
  });

  it('Deberia borrar una tarea', () => {
    let id = component.contacto.controls['id'];
    let user_id = component.contacto.controls['user_id'];
    let title = component.contacto.controls['title'];
    let due_on = component.contacto.controls['due_on'];
    let status = component.contacto.controls['status'];

    id.setValue(1);
    user_id.setValue(79);
    title.setValue('Utilis comminor considero atrocitas deleniti antea tametsi speciosus.');
    due_on.setValue('2021-09-30T00:00:00.000+05:30');
    status.setValue('pending');

    component.crearTarea();
    let cantidad = component.tareas.length;
    expect(component.tareas.length).toBe(cantidad);
    component.borrarTarea(tarea);
    expect(component.tareas.length).toBeLessThan(cantidad);


  });


  it('Comprueba si el array aumenta', waitForAsync(
    () => {

      expect(component.tareas.length).toBeGreaterThanOrEqual(0);

      component.tareas.push({ id: 2065, user_id: 1512, title: "Mrs. Oscar Gibson", due_on: "2021-10-06T00:00:00.000+05:30", status: "pending" });
      expect(component.tareas.length).toBeGreaterThanOrEqual(1);
      component.tareas.push({ id: 2065, user_id: 1512, title: "Mrs. Oscar Gibson", due_on: "2021-10-09T00:00:00.000+05:30", status: "pending" })
      component.tareas.push({ id: 68, user_id: 44, title: "Alias bonus balbus artificiose video.", due_on: "2021-10-09T00:00:00.000+05:30", status: "pending" });
    })
  );


  it('Comprueba si genera los input del formulario', () => {
    const formEle = fixture.debugElement.nativeElement.querySelector('.form-group');
    const inputEle = formEle.querySelectorAll('input');
    expect(inputEle.length).toEqual(4);

  });

  it('Comprueba si genera mi Select y Options del formulario', () => {
    const formSelOpt = fixture.debugElement.nativeElement.querySelector('.form-group');

    const inputSelOpt = formSelOpt.querySelectorAll('select');
    expect(inputSelOpt.length).toEqual(1);

  });

  it('prueba de crear Tareas ',()=>{

    const fixture = TestBed.createComponent(HomeComponent);
    const component = fixture.componentInstance
    fixture.detectChanges();

    let id = component.contacto.controls['id'];
    let user_id = component.contacto.controls['user_id'];
    let title = component.contacto.controls['title'];
    let due_on = component.contacto.controls['due_on'];
    let status = component.contacto.controls['status'];

    id.setValue(1);
    user_id.setValue(79);
    title.setValue('Utilis comminor considero atrocitas deleniti antea tametsi speciosus.');
    due_on.setValue('2021-09-30T00:00:00.000+05:30');
    status.setValue('pending');

    expect(component.contacto.valid).toBeTrue();

  })

});

