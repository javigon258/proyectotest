import { Component, Injectable, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Datos } from 'src/app/models/datos';
import { DatosService } from 'src/app/services/datos.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  tareas: Datos[] = [];
  contacto!: FormGroup;
  private formBuilder: FormBuilder = new FormBuilder;

  submitted = false;

  constructor(public _datosSer: DatosService) {
  }

  borrarTarea(ind: Datos) {
    const index = this.tareas.findIndex(
      (ele: Datos) => ele === ind
    );
    this.tareas.splice(index, 1);
  }

  crearTarea() {
    let nuevaTarea: Datos = {...this.contacto.value};
    this.tareas.push(nuevaTarea);
  }


  ngOnInit(): void {
    this.contacto = this.formBuilder.group({
      id: ['', Validators.required],
      user_id: ['', [Validators.required]],
      title: ['', Validators.required],
      due_on: ['', Validators.required],
      status: ['', Validators.required]
    });
    this.cargarTareas();
  }

  cargarTareas(){
    this._datosSer.getTareas().subscribe(
      (respuesta) => {
         this.tareas = respuesta.data;
         console.log(this.tareas.length);
      }
    )
  }
}
