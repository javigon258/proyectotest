describe('Pruebas de ejemplo', () => {

    it('Comprobar titulo h1', () => {
      cy.visit('/').then(() => {
        cy.contains('Creacion de Tareas');
      });
    });
    
    it('Comprobando al crear una tarea tiene datos', () => {
      cy.visit('/').then(() => {
        cy.get('#idN').type(1);
        cy.get('#user_id').type(11);
        cy.get('#title').type("Prueba");
        cy.get('#due_on').type("2021-09-23");
        cy.get('#status').select("pending");
        cy.get('#crea').click();
        cy.get(':nth-child(21) > :nth-child(4)').should('have.text', 'Prueba');
      });
    });

    it('Comprobando que al hacer click, borrar una tarea', () => {
      cy.visit('/').then(() => {
          cy.contains('borrar').click();
        cy.get(':nth-child(21) > :nth-child(4)').should('not.exist');
      });
    });

  });
  